- header: Own websites
  level: 2
  slug: own

- links:
    - name: Digital Words
      url: https://digitalwords.net/
      icon: dw.png
      desc: my website, where I write about things that interest me.
      langs:
        - he
        - en

    - name: My academic webpage
      url: https://ac.digitalwords.net/
      icon: ac.png
      desc: I’m a linguist, I reverse-engineer language.

    - name: Pušišit
      url: https://xpr.digitalwords.net/
      icon: xpr.png
      desc: my one-person publishing house, where I used to publish things that interest me.
      langs:
        - he
        - eo
      status: inactive

    - name: aˈkit͡sər
      url: https://kcr.digitalwords.net/
      icon: kcr.png
      desc: another blog, where I used to write synopses of articles and books in linguistics (as a memory aid).
      langs:
        - he
        - en
      status: inactive

    - name: Náremardë
      url: https://tolkien.digitalwords.net/naremarde/
      icon: náremardë.webp
      desc: A website about [conlangs](https://en.wikipedia.org/wiki/Constructed_language), and the ones developed by Tolkien in particular.
      langs:
        - en
        - he
      status: inactive

- header: Accounts
  level: 2

- header: Fediverse
  level: 3
  icon: fediverse.svg
  slug: fediverse
  no-margin-top: True

- text: "The [Fediverse](https://en.wikipedia.org/wiki/Fediverse) is an interconnected, federated and decentralised network of social networks. Each node (instance) is independent and communication between them is seamlessly done using common protocols, chielfy [ActivityPub](https://activitypub.rocks/). It is *so* much better and more fun than isolated, centralised and capitalist (anti-)social networks, which are walled gardens. The Internet in its very essence is decentralised; centralisation is the anomaly."

- links:
    - name: tooot.im
      url: https://tooot.im/@rwmpelstilzchen
      icon: tooot.im.webp
      desc: A Hebrew account ([Mastodon](https://joinmastodon.org/)).
      langs:
        - he

    - name: Blåhaj zone
      url: https://blahaj.zone/@serlwch
      icon: blåhaj.webp
      desc: An English account ([Calckey](https://calckey.org/)).
      langs:
        - en

    - name: Tŵt Cymru
      url: https://toot.wales/@rwmpelstilzchen
      icon: tŵt.webp
      desc: A Welsh account (Mastodon).
      langs:
        - cy

    - name: Fribygda
      url: https://fribygda.no/@serlwch
      icon: fribygda.webp
      desc: A Norwegian account (Mastodon).
      langs:
        - nb

    - name: esperanto.masto.host
      url: https://esperanto.masto.host/@rwmpelstilzchen
      desc: An Esperanto account (Mastodon).
      langs:
        - eo

    - name: hayu.sh
      url: https://hayu.sh/@inmediasres/
      icon: inmediasres.webp
      desc: A media diary ([Pleroma](https://pleroma.social/)).
      langs:
        - he

    - name: Wyrms.de
      url: https://wyrms.de/user/rwmpelstilzchen
      icon: wyrms.webp
      desc: Books I read or listened to ([BookWyrm](https://joinbookwyrm.com/)).

    - name: Metapixl
      url: https://metapixl.com/@chi
      icon: chi.webp
      desc: Photos I took ([Pixelfed](https://pixelfed.org/)).

    - name: diasp.org
      url: https://diasp.org/u/rumpelstilzchen
      icon: diaspora.webp
      desc: 'An account on [diaspora\*](https://diasporafoundation.org/). I left diaspora\* because it had no critical mass of people and activity, due to several reasons. I prefer [ActivityPub](https://activitypub.rocks/)-based networks much better, as they are all interconnected; but we must not forget ActivityPub owns its success *inter alia* to the foundations layed by previous protocols and the communities which used these protocols.'
      langs:
        - en
      status: inactive

- header: Contact
  slug: contact
  level: 3
  icon: mirc.webp

- links:
    - name: "[Matrix]"
      url: https://matrix.to/#/@maja:catgirl.cloud
      icon: matrix.svg
      desc: "My IM/VoIP network of choice: an open-standard, federated, fully encrypted protocol."

    - name: Signal
      url: https://signal.org/
      icon: signal.svg
      desc: Signal is centralised, so it is less favourable than [Matrix].
        To contact me, add me as a contact with my phone number (<tt>+972 52-558-7769</tt>).

    - name: Email
      url: mailto:foo@digitalwords.net
      icon: mutt.png
      desc: <tt>foo@digitalwords.net</tt>

    - name: Telephone
      url: phone:+972525587769 # +972-2-6419913 / +972-54-6509361
      icon: telephone.svg
      desc: <tt>+972 52-5587769</tt>

    - name: Telegram
      url: https://t.me/judaronen
      icon: telegram.svg
      desc: My only use for Telegram is to communicate with people who don’t use Signal.
      status: inactive

    - name: ICQ
      url: https://icq.im/11872498
      icon: icq.png
      desc: Free For Chat 🙂
      status: inactive

- header: Git repositories
  level: 3
  icon: git.svg

- links:
    - name: GitLab
      url: https://gitlab.com/u/rwmpelstilzchen
      icon: gitlab.png
      desc: repositories of source code, LaTeX documents, etc.

    - name: GitHub
      url: https://github.com/rwmpelstilzchen
      icon: github.png
      desc: contributions to projects, as well as an archive of my old repositories of source code, LaTeX documents, etc.

- header: Academia
  level: 3
  icon: microscope.svg

- links:
    - name: The Linguist List
      url: https://old.linguistlist.org/people/personal/get-personal-page2.cfm?PersonID=155237
      icon: linguistlist.png

    - name: Academia.edu
      url: http://huji.academia.edu/JudaRonen
      icon: academia.edu.svg
      status: inactive

- header: Music
  level: 3
  icon: euterpe.webp

- links:
    - name: Mutopia Project
      url: http://www.mutopiaproject.org/cgibin/make-table.cgi?searchingfor=me.digitalwords.net
      icon: mutopia.png
      desc: beautifully-engraved free scores.

    - name: The Session
      url: https://thesession.org/members/3610/
      icon: thesession.png
      desc: the best source for Irish folk music scores.

    - name: IMSLP
      url: http://imslp.org/wiki/User:Rumpelstilzchen
      icon: imslp.png
      desc: the best source for classical music scores.

    - name: MuseScore
      url: https://musescore.com/user/4541286/
      icon: musescore.png

    - name: Bandcamp
      url: https://bandcamp.com/rwmpelstilzchen
      icon: bc.svg

- header: Gaming
  level: 3
  icon: megason.webp

- links:
    - name: Lexaloffle BBS
      url: http://www.lexaloffle.com/bbs/?uid=13720
      icon: lexaloffle.png
      desc: PICO-8 ❤️

    - name: itch.io
      url: https://rwmpelstilzchen.itch.io/
      icon: itchio.png

    - name: GOG
      url: https://www.gog.com/u/Rwmpelstilzchen
      icon: gog.svg

- header: Item sharing
  level: 3
  icon: mutualism.webp

- links:
    - name: Agora
      url: http://www.agora.co.il/
      icon: agora.png
      desc: "exchange stuff for free: [things I give](http://www.agora.co.il/toGet.asp?searchType=userSearch&dealType=1&iseek=61996); [things I want](http://www.agora.co.il/toGet.asp?searchType=userSearch&dealType=2&iseek=61996)."
      langs:
        - he

    - name: BookCrossing
      url: http://www.bookcrossing.com/mybookshelf/Rwmpelstilzchen/
      icon: bookcrossing.png
      desc: set books free.

- header: File sharing
  level: 3
  icon: napster.webp

- links:
    - name: JPopsuki 2.0
      url: https://jpopsuki.eu/user.php?id=60834
      icon: jpopsuki.png

    - name: Demonoid
      # url:  https://www.demonoid.pw/users/Rwmpelstilzchen
      url: https://www.demonoid.is/users/Rwmpelstilzchen
      icon: demonoid.png

- header: Crowdfunding
  level: 3
  icon: coin.svg

- links:
    - name: Patreon
      url: https://www.patreon.com/user?u=8349916
      icon: patreon.png
    - name: Kickstarter
      url: https://www.kickstarter.com/profile/1070313606
      icon: kickstarter.png
    - name: Jumpstarter
      url: http://www.jumpstarter.co.il/private_promote.asp?uid={8FF1ACBD-5A6C-4C76-8885-860E7216CC24}
      icon: jumpstarter.png
      langs:
        - he
      status: offline

- header: Forums
  level: 3
  icon: forum.webp

- links:
    - name: Anki Forums
      url: https://forums.ankiweb.net/u/rwmpelstilzchen/
      icon: anki.svg

    - name: Reddit
      url: https://www.reddit.com/user/Rwmpelstilzchen/
      icon: reddit.png
      desc: if it exists, there is a subreddit for it.
      status: inactive

    - name: Beofen Tivʾi
      url: http://beofen-tv.co.il/cgi-bin/chiq.pl?%E9%E5%E3%E4_%F8%E5%F0%EF
      icon: beofen.png
      desc: an online community of unschoolers.
      langs:
        - he
      status: inactive

    - name: Tapuz
      url: http://www.tapuz.co.il/olamot/UserProfile.aspx?profileID=177442
      icon: tapuz.png
      langs:
        - he
      status: inactive

- header: Archives
  level: 3
  icon: cardbox.svg

- links:
    - name: Internet Archive
      url: https://archive.org/details/@j_da_ron_n
      icon: archive.png
      desc: Alexandria 2.0.

    - name: Israeli Digital Archive
      url: https://www.digital-archive.org.il/category/donated-by/juda-ronen/
      icon: hanan.webp
      desc: items I contributed to the archive.

- header: Misc.
  level: 3
  icon: links.svg

- links:
    - name: Wikimedia
      url: https://www.wikimedia.org/
      icon: wikimedia.png
      desc: 'I have a love-hate relationship with Wikimedia’s projects: on the one hand most are wonderful collaborative endeavours, but on the other hand some are ruled by malevolent cliques (notably the Hebrew Wikipedia, which is very hostile and has an extreme right tone). My user pages: the <a href="https://he.wikipedia.org/wiki/%D7%9E%D7%A9%D7%AA%D7%9E%D7%A9:The_Fool">Hebrew</a>, <a href="https://en.wikipedia.org/wiki/User:Rumpelstilzchen">English</a>, <a href="https://cy.wikipedia.org/wiki/Defnyddiwr:Rumpelstilzchen">Welsh</a> and <a href="https://eo.wikipedia.org/wiki/Uzanto:Rumpelstilzchen">Esperanto</a> Wikipedias, the <a href="https://en.wikisource.org/wiki/User:Rumpelstilzchen">English</a> and <a href="https://cy.wikisource.org/wiki/Defnyddiwr:Rumpelstilzchen">Welsh</a> Wikisource, the <a href="https://he.wikibooks.org/wiki/%D7%9E%D7%A9%D7%AA%D7%9E%D7%A9:Rumpelstilzchen">Hebrew</a> Wikibooks, and <a href="https://commons.wikimedia.org/wiki/User:Rumpelstilzchen">Wikimedia Commons</a>.'

    - name: Ravelry
      url: http://www.ravelry.com/people/rwmpelstilzchen
      icon: ravelry.png
      desc: yarn + hook = ♥

    - name: Stack Exchange
      url: http://stackexchange.com/users/3978580/
      icon: se.png
      desc: Q? A!

    - name: YouTube
      url: https://www.youtube.com/channel/UC3ZTeDV4wFXbPeSDJblTNLA # https://www.youtube.com/@judaronen3132/
      icon: youtube.svg
      desc: I don’t have any videos, but you can see what [channels](https://www.youtube.com/@judaronen3132/channels) I’m subscribed to. # https://www.youtube.com/playlist?list=LL3ZTeDV4wFXbPeSDJblTNLA

    - name: Anki
      url: https://ankiweb.net/shared/byauthor/702922008
      icon: anki.svg
      desc: Mnemosyne’s gift to humankind.
      # <a class="site" href="https://ankisrs.net/">Anki</a>: learn and remember stuff effectively. I’ve shared two Anki decks so far: <a href="https://ankiweb.net/shared/info/992960742">Hebrew linguistic terms</a>, with almost 1300 notes, and <a href="https://ankiweb.net/shared/info/862690571">Ælfric’s Preface to his Lives of Saints</a>.

    - name: Tatoeba
      url: https://tatoeba.org/eng/user/profile/Rwmpelstilzchen
      icon: tatoeba.svg
      desc: An multilingual interconnected network of example sentences.

    - name: BOINC
      url: http://boincstats.com/en/stats/-1/user/detail/089e8de2407f853ddc9d08325718efd4/
      icon: boinc.png
      desc: contribute to distributed computing + heat your home during wintertime.

    - name: Geocaching.com
      url: https://www.geocaching.com/p/default.aspx?guid=2c5f088d-020d-44d7-8811-d117c1097236
      icon: geocaching.png

    - name: eBay
      url: https://www.ebay.com/usr/rwmpelstilzchen
      icon: ebay.svg

    - name: VimGolf
      url: https://www.vimgolf.com/29323/JudaRonen
      icon: vimgolf.png
      desc: Every keystroke counts.

    - name: Vim
      url: http://www.vim.org/account/profile.php?user_id=52829
      icon: vim.png
      desc: "*Vim is the greatest editor since the stone chisel.*&emsp;— Jose Unpingco"
      status: inactive

    - name: .share
      url: http://dotshare.it/~Rumpelstilzchen/
      icon: dotshareit.png
      desc: $HOME is where your dotfiles are.
      status: inactive

    - name: Sarahah
      url: https://rwmpelstilzchen.sarahah.com/
      icon: sarahah.png
      desc: send me an anonymous message.
      status: offline

- header: Centralised, non-federated and capitalist general-purpose social networks
  level: 3
  icon: closed_garden.svg

- links:
    - name: Facebook
      url: https://www.facebook.com/juda.ronen
      icon: fb.png
      langs:
        - he

    - name: Twitter
      url: https://twitter.com/JudaRonen
      icon: twitter.svg
      langs:
        - he
        - en
      status: inactive

- header: Family
  level: 2

- links:
    - icon: github.png
      desc: Ilʾil’s <a class="site" href="https://github.com/ilil">GitHub</a> repositories.
    - icon: reem.png
      desc: 'Reʾem’s websites: <a class="site" href="https://reem.digitalwords.net/">old</a> and <a class="site" href="https://reem.digitalwords.net/wp">new</a>.'
    - icon: scratch.png
      desc: <a href="https://scratch.mit.edu/users/Ilil_Gilboa/">Ilʾil</a> and <a href="https://scratch.mit.edu/users/Reem_Gilboa/">Reʾem</a>’s creations in <a class="site" href="https://scratch.mit.edu/">Scratch</a>.
    - icon: ravelry.png
      desc: <a href="http://www.ravelry.com/people/ililgilboa/">Ilʾil</a> and <a href="http://www.ravelry.com/people/reemgilboa">Reʾem</a>’s <a class="site" href="http://www.ravelry.com/">Ravelry</a> accounts.
    - icon: twt.png
      desc: My kids’ cute <a class="site" href="https://kishkush.net/@zaatutroll">toots</a>.
      langs:
        - he

- header: Sites I maintained <small>(all inactive or offline now)</small>
  level: 2

- links:
    - name: The Israeli Tolkien Community
      url: https://tolkien.digitalwords.net/tolkien.co.il/
      icon: tolkienil.webp
      status: inactive

    - icon: ginateva.png
      desc: 'The <a class="site" href="http://ginateva.net/">community garden</a> located in the Jerusalem Natural History Museum yard. The original address is offline now, but another version is available [here](http://gina.digitalwords.net/) (albeit with broken style).'
      langs:
        - he
      status: offline
