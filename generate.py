import dominate
from dominate import tags
from dominate.util import raw
import markdown
import re
import yaml


def md(mdstring):
    #  https://stackoverflow.com/a/63866001/3302752
    return raw(
        re.sub(
            "(^<P>|</P>$)",
            "",
            markdown.markdown(mdstring, extensions=["extra"]),
            flags=re.IGNORECASE,
        )
    )


#  print(links)


def typeset_header(header):
    if header["level"] == 2:
        hfunc = tags.h2
    elif header["level"] == 3:
        hfunc = tags.h3
    else:
        raise Exception("Unsupported header level")
    with hfunc(
        id=header["slug"] if "slug" in header.keys() else "",
        style="margin-top: 0px"
        if "no-margin-top" in header.keys() and header["no-margin-top"]
        else "",
    ):
        if "icon" in header.keys():
            tags.span(tags.img(src=f'pix/{header["icon"]}'), cls="marginpar pix")
        if "slug" in header.keys():
            tags.a(md(header["header"]), href="#" + header["slug"])
        else:
            md(header["header"])


def typeset_langs(langs):
    langcodes = {
        "he": "Hebrew",
        "en": "English",
        "cy": "Welsh",
        "nb": "Norwegian",
        "eo": "Esperanto",
    }
    with tags.span(cls="lang", __pretty=False):
        tags.span(" [")
        first = True
        for lang in langs:
            if first:
                first = False
            else:
                tags.span(", ")
            tags.abbr(lang, title=langcodes[lang])
        tags.span("]")


def typeset_status(status):
    tags.span("⦾  ", status, cls="status")


def typeset_links(links):
    with tags.ul(cls="sitelist"):
        for link in links:
            with tags.li(cls=link["status"] if "status" in link.keys() else "active"):
                with tags.div(cls="pix"):
                    if "icon" in link.keys():
                        if "url" in link.keys():
                            tags.a(
                                tags.img(src=f'pix/{link["icon"]}'), href=link["url"]
                            )
                        else:
                            tags.img(src=f'pix/{link["icon"]}')
                with tags.div(cls="desc", __pretty=False):
                    if "name" in link.keys():
                        tags.a(link["name"], cls="site", href=link["url"])
                    if "desc" in link.keys():
                        tags.span(
                            ": " if "name" in link.keys() else "", md(link["desc"])
                        )
                    if "langs" in link.keys():
                        typeset_langs(link["langs"])
                    if "status" in link.keys():
                        typeset_status(link["status"])


def typeset_text(text):
    tags.p(tags.small(md(text)))


doc = dominate.document(title="Maja Abramski-Kronenberg", lang="en")
with doc.head:
    raw(open("html/head.html", "r").read())
with doc.body:
    raw(open("html/header.html", "r").read())
    taglist = list(yaml.load_all(open("tags.yaml"), Loader=yaml.SafeLoader))[0]
    with tags.div(id="tagscontainer"):
        tags.p(
            "This list is contains a small selection of things I find interesting, values I hold and labels I identify with. It is shuffled every time it is loaded, so the order doesn’t mean anything.",
            cls="meta",
        )
        with tags.ul(id="taglist"):
            for item in taglist:
                with tags.li():
                    if not ("wiki" in item.keys() or "url" in item.keys()):
                        url = "https://en.wikipedia.org/wiki/" + item["name"]
                    elif "wiki" in item.keys():
                        url = "https://en.wikipedia.org/wiki/" + item["wiki"]
                    else:  # 'url'
                        url = item["url"]
                    tags.a(str(item.get("emoji")) + " " + item["name"], href=url)
                    if "note" in item.keys():
                        tags.span(" (", md(item["note"]), ")", cls="tagnote")
    raw(open("html/header-nonactive.html", "r").read())

    links = list(yaml.load_all(open("links.yaml"), Loader=yaml.SafeLoader))[0]
    for item in links:
        if "header" in item.keys():
            typeset_header(item)
        if "links" in item.keys():
            typeset_links(item["links"])
        if "text" in item.keys():
            typeset_text(item["text"])
    raw(open("html/footer.html", "r").read())
print(doc)
